using System.Diagnostics;

namespace ThreadingForms
{
    public partial class Form1 : Form
    {
        public string Time { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var timer = new Stopwatch();

            Task taksInt = new Task(() =>
            {

                System.Diagnostics.Debug.WriteLine("iu");
                try
                {
                    timer.Start();
                    for (int i = 0; i < 100; i++)
                    {
                        listBox1.Invoke((MethodInvoker)delegate
                        {
                            listBox1.Items.Add(i);
                            //int visibleItems = listBox1.ClientSize.Height / listBox1.ItemHeight;
                            //listBox1.TopIndex = Math.Max(listBox1.Items.Count - visibleItems + 1, 0);
                        });
                    }

                    timer.Stop();
                    label1.Invoke((MethodInvoker)delegate
                    {
                        label1.Text = timer.ElapsedMilliseconds.ToString();
                    });
                }
                catch (Exception ex)
                {

                    System.Diagnostics.Debug.WriteLine(ex);
                    throw;
                }
            });
            taksInt.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var timer = new Stopwatch();
            System.Diagnostics.Debug.WriteLine("##########");
            timer.Start();
            for (int i = 0; i < 100; i++)
            {
                listBox2.Items.Add(i);
                int visibleItems = listBox2.ClientSize.Height / listBox2.ItemHeight;
                listBox2.TopIndex = Math.Max(listBox2.Items.Count - visibleItems + 1, 0);
            }
            timer.Stop();
            label2.Text = timer.ElapsedMilliseconds.ToString();
        }
    }
}