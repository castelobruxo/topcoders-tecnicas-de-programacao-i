﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace LearningLinq
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using var reader = new StreamReader("./data.json");
            string json = reader.ReadToEnd();
            var data = JsonSerializer.Deserialize<List<Person>>(json);

            var result = data
                .Where(x => x.Age < 18)
                .OrderBy(x => x.Age)
                .ElementAt(1);


            Console.WriteLine(result);  
        }
    }
}