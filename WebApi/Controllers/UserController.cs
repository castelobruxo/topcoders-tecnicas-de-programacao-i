using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> logger;
        private readonly IConfiguration configuration;

        public UserController(ILogger<UserController> logger, IConfiguration configuration)
        {
            this.logger = logger;
            this.configuration = configuration;
        }

        [HttpGet]
        public async Task<ActionResult<object>> Get([FromHeader] string apiKey, [FromQuery] string filter, [FromQuery] string orderBy, [FromQuery] int page = 1, [FromQuery] int pageSize = 10)
        {
            if (apiKey != configuration.GetValue<string>("ApiKey"))
            {
                return Unauthorized();
            }

            using var reader = new StreamReader("./data.json");
            var json = await reader.ReadToEndAsync();
            var data = JsonSerializer.Deserialize<List<User>>(json);

            var filteredData = data;

            if (!string.IsNullOrWhiteSpace(filter))
            {
                filteredData = data
                    .Where(x => x.FirstName == filter).ToList();
            }

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                var propertyInfo = typeof(User).GetProperty(orderBy);
                filteredData = filteredData.OrderBy(x => propertyInfo.GetValue(x, null)).ToList();
            }

            return Ok(new
            {
                StatusCode = 200,
                Message = "Deu tudo certo",
                Meta = new
                {
                    CurrentPage = page,
                    PageSize = pageSize
                },
                Data = filteredData
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            using var reader = new StreamReader("./data.json");
            var json = await reader.ReadToEndAsync();
            var data = JsonSerializer.Deserialize<List<User>>(json);

            var result = data.Where(x => x.Id == id).FirstOrDefault();

            if (result == null)
            {
                return Ok(new
                {
                    message = "Irm�o, nem tem viu"
                });
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<object>> CreateUser([FromHeader] string apiKey, [FromBody] CreateUser request)
        {
            if (apiKey != configuration.GetValue<string>("ApiKey"))
            {
                return Unauthorized();
            }

            var reader = new StreamReader("./data.json");
            var json = reader.ReadToEnd();
            reader.Dispose();
            var data = JsonSerializer.Deserialize<List<User>>(json);

            var lastId = data.OrderBy(x => x.Id).Last().Id + 1;
            var user = new User
            {
                Id = lastId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Gender = request.Gender,
                Age = (int)DateTime.Now.Subtract(request.Birthday).TotalDays / 365
            };

            data.Add(user);
            var content = JsonSerializer.Serialize(data);
            System.IO.File.WriteAllText("./data.json", content);

            return Created($"https://localhost:44332/users/{lastId}", user);
        }
    }
}