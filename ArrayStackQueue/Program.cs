﻿using System;
using System.Collections.Generic;

namespace ArrayStackQueue
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var endApp = false;
            var priorityQueue = new Queue<string>();
            var regularQueue = new Queue<string>();
            var regularAttendenceCount = 0;
            var priorityAttendenceCount = 0;
            var count = 0;

            while (!endApp)
            {
                Console.WriteLine("#### Escolha uma das opções ####");
                Console.WriteLine("1 - Emitir senha regular");
                Console.WriteLine("2 - Emitir senha prioritária");
                Console.WriteLine("3 - Realizar atendimento");
                Console.WriteLine("4 - Sair");

                switch (Console.ReadLine())
                {
                    case "1":
                        regularQueue.Enqueue($"R{regularAttendenceCount + 1}");
                        regularAttendenceCount++;
                        Console.WriteLine($"Senha emitida: R{regularAttendenceCount}");
                        break;
                    case "2":
                        priorityQueue.Enqueue($"P{priorityAttendenceCount + 1}");
                        priorityAttendenceCount++;
                        Console.WriteLine($"Senha emitida: P{priorityAttendenceCount}");
                        break;
                    case "3":
                        if (count >= 2 && regularQueue.Count > 0)
                        {
                            Console.WriteLine($"Atendimento: {regularQueue.Dequeue()}");
                            count = 0;
                        }
                        else if (priorityQueue.Count > 0)
                        {
                            Console.WriteLine($"Atendimento: {priorityQueue.Dequeue()}");
                            count++;
                        }
                        else if (regularQueue.Count > 0)
                        {
                            Console.WriteLine($"Atendimento: {regularQueue.Dequeue()}");
                            count++;
                        }
                        else
                        {
                            Console.WriteLine("Filas vazias");
                        }

                        break;
                    case "4":
                        endApp = true;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}