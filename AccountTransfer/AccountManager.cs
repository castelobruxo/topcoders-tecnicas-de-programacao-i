﻿//using AccountTransfer;
//using System;
//using System.Threading;

//namespace AccountTransfer
//{
//    public class AccountManager
//    {
//        private Account FromAccount;
//        private Account ToAccount;
//        private double TransferAmount;
//        public AccountManager(Account AccountFrom, Account AccountTo, double AmountTransfer)
//        {
//            FromAccount = AccountFrom;
//            ToAccount = AccountTo;
//            TransferAmount = AmountTransfer;
//        }
//        public void FundTransfer()
//        {
//            Console.WriteLine($"{Thread.CurrentThread.Name} trying to acquire lock on {FromAccount.Id}");
//            lock (FromAccount)
//            {
//                Console.WriteLine($"{Thread.CurrentThread.Name} acquired lock on {FromAccount.Id}");
//                Console.WriteLine($"{Thread.CurrentThread.Name} Doing Some work");
//                Thread.Sleep(1000);
//                Console.WriteLine($"{Thread.CurrentThread.Name} trying to acquire lock on {ToAccount.Id}");
//                lock (ToAccount)
//                {
//                    FromAccount.WithdrawMoney(TransferAmount);
//                    ToAccount.DepositMoney(TransferAmount);
//                }
//            }
//        }
//    }
//}



//using AccountTransfer;
//using System;
//using System.Threading;

//namespace AccountTransfer
//{
//    public class AccountManager
//    {
//        private Account FromAccount;
//        private Account ToAccount;
//        private double TransferAmount;
//        public AccountManager(Account AccountFrom, Account AccountTo, double AmountTransfer)
//        {
//            this.FromAccount = AccountFrom;
//            this.ToAccount = AccountTo;
//            this.TransferAmount = AmountTransfer;
//        }
//        public void FundTransfer()
//        {
//            Console.WriteLine($"{Thread.CurrentThread.Name} trying to acquire lock on {FromAccount.Id}");

//            lock (FromAccount)
//            {
//                Console.WriteLine($"{Thread.CurrentThread.Name} acquired lock on {FromAccount.Id}");
//                Console.WriteLine($"{Thread.CurrentThread.Name} Doing Some work");
//                Thread.Sleep(3000);
//                Console.WriteLine($"{Thread.CurrentThread.Name} trying to acquire lock on {ToAccount.Id}");

//                if (Monitor.TryEnter(ToAccount, 3000))
//                {
//                    Console.WriteLine($"{Thread.CurrentThread.Name} acquired lock on {ToAccount.Id}");
//                    try
//                    {
//                        FromAccount.WithdrawMoney(TransferAmount);
//                        ToAccount.DepositMoney(TransferAmount);
//                    }
//                    finally
//                    {
//                        Monitor.Exit(ToAccount);
//                    }
//                }
//                else
//                {
//                    Console.WriteLine($"{Thread.CurrentThread.Name} Unable to acquire lock on {ToAccount.Id}, So existing.");
//                }
//            }
//        }
//    }
//}



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AccountTransfer
{
    public class AccountManager
    {
        private Account FromAccount;
        private Account ToAccount;
        private double TransferAmount;

        public AccountManager(Account AccountFrom, Account AccountTo, double AmountTransfer)
        {
            FromAccount = AccountFrom;
            ToAccount = AccountTo;
            TransferAmount = AmountTransfer;
        }
        public void FundTransfer()
        {
            object _lock1, _lock2;
            if (FromAccount.Id < ToAccount.Id)
            {
                _lock1 = FromAccount;
                _lock2 = ToAccount;
            }
            else
            {
                _lock1 = ToAccount;
                _lock2 = FromAccount;
            }
            Console.WriteLine($"{Thread.CurrentThread.Name} trying to acquire lock on {((Account)_lock1).Id}");

            lock (_lock1)
            {
                Console.WriteLine($"{Thread.CurrentThread.Name} acquired lock on {((Account)_lock1).Id}");
                Console.WriteLine($"{Thread.CurrentThread.Name} Doing Some work");
                Thread.Sleep(3000);
                Console.WriteLine($"{Thread.CurrentThread.Name} trying to acquire lock on {((Account)_lock2).Id}");
                lock (_lock2)
                {
                    Console.WriteLine($"{Thread.CurrentThread.Name} acquired lock on {((Account)_lock2).Id}");
                    Console.WriteLine($"Transfering money.....");
                    FromAccount.WithdrawMoney(TransferAmount);
                    ToAccount.DepositMoney(TransferAmount);
                    Console.WriteLine($"{Thread.CurrentThread.Name} releasing {((Account)_lock2).Id}");
                }
                Console.WriteLine($"{Thread.CurrentThread.Name} releasing {((Account)_lock1).Id}");
            }
        }
    }
}