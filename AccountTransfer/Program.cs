﻿using System.Threading;
using System;

namespace AccountTransfer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main Thread Started");
            Account Account1 = new Account(1, 5000);
            Account Account2 = new Account(2, 3000);
            AccountManager accountManager1 = new AccountManager(Account1, Account2, 5000);
            AccountManager accountManager2 = new AccountManager(Account2, Account1, 6000);


            Thread thread1 = new Thread(accountManager1.FundTransfer)
            {
                Name = "Thread1"
            };

            Thread thread2 = new Thread(accountManager2.FundTransfer)
            {
                Name = "Thread2"
            };

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();

            Console.WriteLine("Main Thread Completed");
            Console.ReadKey();
        }
    }
}