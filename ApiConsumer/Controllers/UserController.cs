using Microsoft.AspNetCore.Mvc;
using Models;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace ApiConsumer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public UserController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet]
        public async Task<object> Get()
        {
            var secret = configuration.GetValue<string>("ApiKey");
            var baseUrl = configuration.GetValue<string>("BaseUrl");

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("apiKey", secret);

            try
            {
                var body = new CreateUser
                {
                    FirstName = "Pedrim",
                    LastName = "Aleatório",
                    Email = "pedrim@bol.com.br", 
                    Gender = "Male",
                    Birthday = DateTime.Now.AddYears(-10)
                };

                var json = JsonSerializer.Serialize(body);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpClient.PutAsync($"{baseUrl}/users", data);

                var code = response.StatusCode;
                var content = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<JsonObject>(content);
                var id = result["id"]["asd"];
                System.Diagnostics.Debug.WriteLine(id);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return null;
        }
    }
}