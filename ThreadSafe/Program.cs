﻿using System;
using System.Threading;

namespace ThreadSafe
{
    public class Program
    {
        static readonly string input = "THE MATRIX HAS YOU";
        static object DodgeThis = new object();
        static Mutex mutex = new Mutex();

        public static void Main(string[] args)
        {
            Thread one = new(PrintSomething);
            Thread two = new(PrintSomething);
            one.Start();
            two.Start();
        }

        public static void PrintSomething()
        {
            mutex.WaitOne();
            for (int i = 0; i < input.Length; i++)
            {
                Console.Write(input[i]);
                var asd = new Random().Next(1000);
                Thread.Sleep(asd);
            }
            mutex.ReleaseMutex();
        }

        public static void PrintSomethingElse()
        {
            for (int i = 0; i < input.Length; i++)
            {
                Console.Write(input[i]);
                Thread.Sleep(500);
            }
        }
    }
}
