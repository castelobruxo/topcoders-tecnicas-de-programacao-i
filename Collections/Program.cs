﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace Collections
{
    internal class Program
    {
        public static void Main()
        {
            using var r = new StreamReader("./data.json");
            string json = r.ReadToEnd();
            var items = JsonSerializer.Deserialize<List<Person>>(json);
        }
    }
}