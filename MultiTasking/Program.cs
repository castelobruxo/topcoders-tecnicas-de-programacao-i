﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace MultiTasking
{
    public class Program
    {
        /*
        public static void Main(string[] args)
        {
            Blah().GetAwaiter().GetResult();
        }
        public static async Task Blah()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Token", "gadfjkshjsdgfk");

            try
            {
                var response = await httpClient.GetAsync("https://api.github.com/users/ronaldosena");

                var code = response.StatusCode;
                var message = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<GithubResponse>(message);
                Console.WriteLine($"result = ");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
        }         
        */
        public static void Main(string[] args)
        {
            Blah().GetAwaiter().GetResult();
        }

        public static async Task Blah()
        {
            var thread1 = new Thread(() =>
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Awesome-Octocat-App");
                try
                {
                    var response = httpClient.GetAsync("https://api.github.com/users/ronaldosena").GetAwaiter().GetResult();
                    var code = response.StatusCode;
                    var message = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    var result = JsonSerializer.Deserialize<GithubResponse>(message);
                    Console.WriteLine(result.avatar_url);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);

                    throw;
                }
            });
            var thread2 = new Thread(() =>
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Awesome-Octocat-App");
                try
                {
                    var response = httpClient.GetAsync("https://api.github.com/users/visionmedia").GetAwaiter().GetResult();
                    var code = response.StatusCode;
                    var message = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    var result = JsonSerializer.Deserialize<GithubResponse>(message);
                    Console.WriteLine(result.avatar_url);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    throw;
                }
            });

            thread1.Start();
            thread2.Start();
        }
    }
}