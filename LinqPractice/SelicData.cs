﻿using System;
using System.Text.Json.Serialization;

namespace LinqPractice
{
    public class SelicData
    {
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }

        [JsonPropertyName("selic")]
        public double Selic { get; set; }
    }
}
