using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace LinqPractice
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using var reader = new StreamReader("./data.json");
            var json = reader.ReadToEnd();
            var data = JsonSerializer.Deserialize<List<SelicData>>(json);

            data = data.OrderBy(x => x.Date).ToList();

            #region Encontre os valores: maior, médio e menor da selic
            var highestValue = data.Max(x => x.Selic);
            var lowestValue = data.Min(x => x.Selic);
            var averageValue = data.Average(x => x.Selic);
            #endregion

            #region Qual o valor mais comum da selic?
            var mode = data
                .GroupBy(x => x.Selic)
                .OrderByDescending(x => x.Count())
                .Select(x => new { Value = x.Key, Count = x.Count() })
                .GroupBy(x => x.Count) // checando por mais de uma moda
                .FirstOrDefault();
            /*
             o objeto de saida será algo como:
                {
                    Key = 497,
                    _elements = [
                        { Value = 6.5, Count = 497 }
                        { Value = 12.5, Count = 497 }
                    ]
                }
             */
            #endregion

            #region Encontre os meses em que houve mudança no valor da selic
            var changingMonths = data
                .Where((item, idx) => idx == 0 || item.Selic != data.ElementAt(idx - 1).Selic);
            #endregion

            #region Qual o valor mais alto e mais baixo da selic para cada presidente da república
            // repita para os outros presidentess
            var dilmaInit = new DateTime(2012, 1, 1);
            var dilmaEnd = new DateTime(2016, 8, 31);
            var dilmaMax = data.Where(x => x.Date > dilmaInit && x.Date < dilmaEnd).Max(x => x.Selic);
            var dilmaMin = data.Where(x => x.Date > dilmaInit && x.Date < dilmaEnd).Min(x => x.Selic);
            #endregion

            #region Calcule o valor médio de cada trimestre a partir de 2016
            var tri = data
                .Where(x => x.Date > new DateTime(2016, 1, 1))
                .GroupBy(x => $"{Math.Ceiling((double)x.Date.Month / 3)}T{x.Date.Year}") // -> e.g. 2T2022
                .Select(x => new { Trimester = x.Key, MeanVal = x.Average(y => y.Selic) })
                .ToList();
            #endregion

            #region Calcule a taxa média de aumento desde março/21
            var filteredData = data
                .Where(x => x.Date > new DateTime(2021, 3, 1)).Select(x => x.Selic).Distinct();
            var rate = filteredData.Zip(filteredData.Skip(1), (a, b) => b - a).Average(x => x);
            #endregion

            #region Encontre o caractere mais frequente de uma string qualquer
            var anystring = "aaaaaabbbcccwwwqqqq";
            var mostCommonChar = anystring.GroupBy(x => x).OrderByDescending(x => x.Count()).First().Key;
            #endregion

            #region Decodificando string
            var chars = "!@#$%¨&*()";
            var input = "######&*";
            var decoded = String.Join("", input.Select(x => chars.IndexOf(x)));
            #endregion

            #region Embaralhando uma lista ordenada
            var orderedList = Enumerable.Range(1, 20);
            var shuffledList = orderedList.OrderBy(i => new Random().Next());
            #endregion

            #region Transpondo uma matriz quadrada 
            var matrix = new List<List<int>>
            {
                 new List<int> {1,1,1,1},
                 new List<int> {2,2,2,2},
                 new List<int> {3,3,3,3},
                 new List<int> {4,4,4,4}
            };
            var transposed = Enumerable.Range(0, matrix.Count).Select(x => matrix.Select(y => y[x])).ToList();
            foreach (var line in matrix)
            {
                foreach (var col in line)
                {
                    Console.Write(col);
                }
                Console.Write("\n");
            }
            foreach (var line in transposed)
            {
                foreach (var col in line)
                {
                    Console.Write(col);
                }
                Console.Write("\n");
            }
            #endregion
        }
    }
}